﻿
namespace Logistics
{
    internal class GeneratableItemTypes
    {
        internal enum Type
        { GOLD, PREMIUM,
          TOTAL }	
    }

    internal class GeneratorTypes
    {
        internal enum Type
        { GOLD_MINOR, GOLD_MAJOR, GOLD_XXL, PREMIUM_MINOR,
          TOTAL }
    }
}
