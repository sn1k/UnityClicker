﻿using System;
using Logistics;


public class GeneratorCurrency
{
    private GeneratableItemTypes.Type m_generatedCurrency;
    private GeneratableItemTypes.Type m_costCurrency;

    private double m_baseCost;
    private double m_costGrowthRate;
    private double m_productionBase;
    private double m_activeMultiplier;

    private int m_numOwned;


    internal GeneratorCurrency(GeneratableItemTypes.Type in_currencyType, GeneratableItemTypes.Type in_costCurrency, double in_baseCost, double in_costGrowthRate, double in_productionBase)
    {
        m_costCurrency = in_costCurrency;
        m_baseCost = in_baseCost;
        m_costGrowthRate = in_costGrowthRate;

        m_generatedCurrency = in_currencyType;
        m_productionBase = in_productionBase;
        m_activeMultiplier = 1.0;
        m_numOwned = 0;
    }

    internal ItemQuantity GenerateCurrency(double in_seconds)
    {
        double amountGenerated = m_productionBase * m_numOwned * m_activeMultiplier * in_seconds;
	    return new ItemQuantity(m_generatedCurrency, amountGenerated);
    }

    internal ItemQuantity GetNextCost()
    {
        ItemQuantity quant = new ItemQuantity(m_costCurrency, m_baseCost);

        if (m_numOwned == 0)
		    return quant;

        quant.m_quantity *= Math.Pow(m_costGrowthRate, m_numOwned);
        
        return quant;
    }

    internal void Build(int in_numToBuild)
    {
        m_numOwned += in_numToBuild;
    }

    internal int GetNumBuilt()
    {
        return m_numOwned;
    }
}
