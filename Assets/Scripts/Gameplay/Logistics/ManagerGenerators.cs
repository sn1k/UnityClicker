﻿using System.Collections.Generic;
using UnityEngine;
using Logistics;
using GenableItemType = Logistics.GeneratableItemTypes.Type;

public class ManagerGenerators : MonoBehaviour
{
    internal delegate void CurrencyUpdatedDelegate(GenableItemType type, double newAmount);
    internal delegate void GeneratorPurchaseDelegate(GeneratorTypes.Type type, int newAmount);


    private static ManagerGenerators s_instance = null;

    private event CurrencyUpdatedDelegate m_currencyUpdatedEvent;
    private event GeneratorPurchaseDelegate m_generatorUpdatedEvent;

    private Dictionary<GeneratorTypes.Type, GeneratorCurrency> m_currencyGenerators = new Dictionary<GeneratorTypes.Type, GeneratorCurrency>();
    
    private Dictionary<GenableItemType, double> m_itemInventory = null;


    private void Awake()
    {
        if (s_instance == null)
        {
            DontDestroyOnLoad(gameObject);
            s_instance = this;
            return;
        }
    }

    internal static ManagerGenerators GetInstance()
    {
        if (s_instance == null)
        {
            Debug.LogError("[ERROR] ManagerGenerators instance not defined");
        }

        return s_instance;
    }


    // Use this for initialization
    private void Start ()
    {
        m_itemInventory = ManagerPlayerInventory.GetInstance().Inventory;

        // TODO MOVE THIS SOMEWHERE NICER
        m_itemInventory[GenableItemType.GOLD] = 2d;

        // fill generators with types available
        m_currencyGenerators[GeneratorTypes.Type.GOLD_MINOR] = new GeneratorCurrency(GenableItemType.GOLD, GenableItemType.GOLD, 2, 0.7d, 0.81d);
        m_currencyGenerators[GeneratorTypes.Type.GOLD_MAJOR] = new GeneratorCurrency(GenableItemType.GOLD, GenableItemType.GOLD, 5, 0.9d, 2.01d);
        m_currencyGenerators[GeneratorTypes.Type.GOLD_XXL] = new GeneratorCurrency(GenableItemType.GOLD, GenableItemType.GOLD, 12, 1.2d, 5.12d);
        m_currencyGenerators[GeneratorTypes.Type.PREMIUM_MINOR] = new GeneratorCurrency(GenableItemType.PREMIUM, GenableItemType.GOLD, 25, 2.5d, 0.2d);
    }
	
	// Update is called once per frame
	private void Update ()
    {
        for(int i = 0; i < (int)GeneratorTypes.Type.TOTAL; ++i)
        {
            GeneratorCurrency generator = m_currencyGenerators[(GeneratorTypes.Type)i];

            if (generator.GetNumBuilt() == 0)
                continue;

            ItemQuantity generatedItem = generator.GenerateCurrency(Time.deltaTime);

            // this is for adding currency generated
            m_itemInventory[generatedItem.m_type] += generatedItem.m_quantity;

            if(m_currencyUpdatedEvent != null)
                m_currencyUpdatedEvent.Invoke(generatedItem.m_type, m_itemInventory[generatedItem.m_type]);
        }
    }

    internal bool CreateGeneratorCurrency(GeneratorTypes.Type in_type)
    {
        GeneratorCurrency generator = m_currencyGenerators[in_type];

        ItemQuantity cost = generator.GetNextCost();

        double available = m_itemInventory[cost.m_type];

        if (available < cost.m_quantity)
            return false;

        generator.Build(1);
        m_itemInventory[cost.m_type] -= cost.m_quantity;

        if (m_generatorUpdatedEvent != null)
            m_generatorUpdatedEvent.Invoke(in_type, generator.GetNumBuilt());

        return true;
    }

    internal void SubscribeCurrencyChangedEvent(CurrencyUpdatedDelegate in_del)
    {
        m_currencyUpdatedEvent += in_del;
    }

    internal void SubscribeGeneratorsChangedEvent(GeneratorPurchaseDelegate in_del)
    {
        m_generatorUpdatedEvent += in_del;
    }
}
