﻿using System.Collections.Generic;
using UnityEngine;
using Logistics;


public class ManagerPlayerInventory : MonoBehaviour
{
    private static ManagerPlayerInventory m_instance = null;

    private Dictionary<GeneratableItemTypes.Type, double> m_inventory = new Dictionary<GeneratableItemTypes.Type, double>();

    internal Dictionary<GeneratableItemTypes.Type, double> Inventory { get { return m_inventory; } }


    private void Awake()
    {
        if (m_instance == null)
        {
            DontDestroyOnLoad(gameObject);
            m_instance = this;

            for (int i = 0; i < (int)GeneratableItemTypes.Type.TOTAL; ++i)
            {
                m_inventory[(GeneratableItemTypes.Type)i] = 0d;
            }

            return;
        }

        Debug.LogError("[Error] ManagerPlayerInventory already have singleton instance");
    }

    internal static ManagerPlayerInventory GetInstance()
    {
        return m_instance;
    }
}
