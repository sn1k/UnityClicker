﻿
namespace Logistics
{
    public class ItemQuantity
    {
        internal GeneratableItemTypes.Type m_type;
        internal double m_quantity;

        internal ItemQuantity(GeneratableItemTypes.Type in_type, double in_quantity)
        {
            m_type = in_type;
            m_quantity = in_quantity;
        }
    }
}