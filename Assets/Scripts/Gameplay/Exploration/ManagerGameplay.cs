﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// Handles encounter generation, player input that modifies them, loot generation.
/// </summary>
public class ManagerGameplay : MonoBehaviour
{
    public delegate void LoggerDelegate(string in_text);
    public delegate void ExitCombatDelegate(bool in_victory);

    private enum STATE_GAMEPLAY { exploring, combat, resting };
    
    public ManagerCombat m_managerCombat;
    public ButtonHandlerExplore m_buttonHandler;
    public ChartboostHandler m_chartboostHandler;

    public Text m_combatLog;
    public ScrollRect m_scrollRect;

    private EntityCombat m_entityPlayer;
    
    private STATE_GAMEPLAY m_currentState = STATE_GAMEPLAY.exploring;

    private float m_lastTickTime;

    private int m_chargeCurrent = 0;
    
    private bool m_paused = false;

    public float m_tickRateSeconds = 1f; // should be client modifiable


    // Use this for initialization
    private void Start ()
    {
        m_lastTickTime = Time.timeSinceLevelLoad;

        DataPlayerStats playerStats = new DataPlayerStats(1, 1, 1, 1, 1);
        DataEquipment playerEquipment = new DataEquipment();

        m_entityPlayer = new EntityCombat(playerStats, playerEquipment, "player", 100f);
        m_entityPlayer.m_equipment.m_handPrimary = new ItemEquipable("woodenSword", ItemEquipable.Slot.HandPrimary, 1.2f, null);

        m_managerCombat.InjectLogger(PrintLog);
        m_managerCombat.InjectCombatCompleted(OnCombatComplete);

        m_buttonHandler.RegisterManagerGameplay(this);
    }
	
	// Update is called once per frame
	private void Update ()
    {
        float timeNow = Time.timeSinceLevelLoad;

        if (timeNow - m_lastTickTime < m_tickRateSeconds)
            return;

        if (m_paused)
            return;

        switch(m_currentState)
        {
            case STATE_GAMEPLAY.resting:
                PrintLog("You rest");
                m_currentState = STATE_GAMEPLAY.exploring;
                break;
            case STATE_GAMEPLAY.exploring:
                RollRandomEncounter();
                break;
            case STATE_GAMEPLAY.combat:
                // let combat manager do his thing
                m_managerCombat.Tick();
                break;
        }

        m_lastTickTime = timeNow;
    }

    internal void Pause()
    {
        m_paused = !m_paused;
    }

    internal void Charge()
    {
        ++m_chargeCurrent;
    }

    internal void Strike()
    {
        if (m_chargeCurrent == 0)
            return;

        m_managerCombat.Strike(m_chargeCurrent);
        m_chargeCurrent = 0;
    }

    internal void ShowAds()
    {
        m_chartboostHandler.RewardVideoShow();
    }

    internal void PrintLog(string in_msg)
    {
        if (m_combatLog == null)
            return;

        m_combatLog.text = string.Format("{0}{1}{2}", m_combatLog.text, System.Environment.NewLine, in_msg);

        // autoscroll to bottom
        m_scrollRect.verticalNormalizedPosition = 0f;
    }

    internal void OnCombatComplete(bool in_victory)
    {
        PrintLog("Combat complete! " + (in_victory?  "Victory!" : "Failure."));
        m_currentState = STATE_GAMEPLAY.resting;
    }

    private void RollRandomEncounter()
    {
        int randRoll = Random.Range(0, 100); // 0-99

        if(randRoll < 33)
        {
            PrintLog("You further explore.");
        }
        else if(randRoll < 90)
        {
            // combat
            m_currentState = STATE_GAMEPLAY.combat;
            
            int difficulty = Random.Range(0, 3); // 0-2

            int numOpponents = Random.Range(difficulty +1, difficulty+2); //1-2, 2-3, 3-4
            
            PrintLog(string.Format("You are engaged in combat by {0} enemies of difficulty {1}", numOpponents, difficulty));

            m_managerCombat.InitialiseCombat(m_entityPlayer, numOpponents, difficulty);
        }
        else
        {
            PrintLog("todo");
            // loot
        }
    }
}
