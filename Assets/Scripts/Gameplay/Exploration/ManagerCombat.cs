﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ManagerCombat : MonoBehaviour
{
    List<EntityCombat> m_enemies = new List<EntityCombat>(4);
    EntityCombat m_player;

    private int m_chargesPending = 0;

    private bool m_playerTurn = true;

    private ManagerGameplay.LoggerDelegate m_log;
    private ManagerGameplay.ExitCombatDelegate m_onExit;


    internal void InjectLogger(ManagerGameplay.LoggerDelegate in_loggerDelegate)
    {
        m_log = in_loggerDelegate;
    }

    internal void InjectCombatCompleted(ManagerGameplay.ExitCombatDelegate in_combatExitDelegate)
    {
        m_onExit = in_combatExitDelegate;
    }


    internal void InitialiseCombat(EntityCombat in_player, int in_numOpponents, int in_difficulty)
    {
        m_player = in_player;

        m_enemies.Clear();

        for(int i = 0; i < in_numOpponents; ++i)
        {
            int enemyStrength = GenerateRandomStatValue(in_difficulty);
            int enemyPerception = GenerateRandomStatValue(in_difficulty);
            int enemyEndurance = GenerateRandomStatValue(in_difficulty);
            int enemyAgility = GenerateRandomStatValue(in_difficulty);
            int enemyLuck = GenerateRandomStatValue(in_difficulty);
            
            DataPlayerStats enemyStats = new DataPlayerStats(enemyStrength, enemyPerception, enemyEndurance, enemyAgility, enemyLuck);
            DataEquipment enemyEquipment = new DataEquipment();

            enemyEquipment.m_handPrimary = new ItemEquipable("wooden sword", ItemEquipable.Slot.HandPrimary, 0.2f, null);
            enemyEquipment.m_chest = new ItemEquipable("wooden chest", ItemEquipable.Slot.Chest, 0.3f, null);
            enemyEquipment.m_legs = new ItemEquipable("wooden legs", ItemEquipable.Slot.Legs, 0.2f, null);
            enemyEquipment.m_boots = new ItemEquipable("wooden boots", ItemEquipable.Slot.Boots, 0.1f, null);
            enemyEquipment.m_head = new ItemEquipable("wooden helm", ItemEquipable.Slot.Head, 0.15f, null);

            m_enemies.Add(new EntityCombat(enemyStats, enemyEquipment, "badguy" + i, (in_difficulty + 2) * 20f));
        }        
    }

    private int GenerateRandomStatValue(int in_difficulty)
    {
        return Random.Range(0, in_difficulty);
    }


    private bool RollToHit(EntityCombat in_attacker, EntityCombat in_defender)
    {
        int agilityDiff = in_attacker.m_stats.m_agility - in_defender.m_stats.m_agility + m_chargesPending;
        float healthModifier = (in_defender.m_health / in_defender.m_healthMax) * 0.5f + 1f; // up to extra 50% to hit if they're basically dead

        float chanceToHit = Mathf.Min(100f, (70f + (agilityDiff * 12.5f)) * healthModifier); // 70 +- diff in 12.5 incs
        float hitRoll = Random.Range(0f, 100f);
        
        if(hitRoll <= chanceToHit)
        {
            return true;
        }

        m_log(string.Format("Missed {0} with {1}! {2} / {3}", m_enemies[0].m_name, m_player.m_equipment.m_handPrimary.m_name, hitRoll.ToString("n2"), chanceToHit.ToString("n2")));
        return false;
    }

    private bool RollToDamage(EntityCombat in_attacker, EntityCombat in_defender, out float out_roll)
    {
        int damageDiff = in_attacker.m_stats.m_strength - in_defender.m_stats.m_endurance + m_chargesPending; // positive good

        float damageModifier = Mathf.Min(100f, 70f + (damageDiff * 12.5f)); // 70 +- diff in 12.5 incs
        out_roll = Random.Range(0f, 100f);
        
        if(out_roll <= damageModifier)
        {
            return true;
        }

        m_log(string.Format("Hit {0}, but with a glancing blow! {1} / {2}", in_defender.m_name, damageModifier.ToString("n2"), out_roll.ToString("n2")));
        return false;
    }

    internal void Strike(int in_charges)
    {
        m_chargesPending += in_charges;
        
        if (m_enemies.Count == 0)
            return;

        Tick();
    }

    internal void Tick()
    {
        if (m_playerTurn)
        {
            // handle dots.

            // handle who we are attacking

            // calc damage with offense, enemy defense, and any status effects
            var enemy = m_enemies[0];

            var enemyStats = enemy.m_stats;

            if ( RollToHit(m_player, enemy) )
            {
                float damageRoll;

                if( RollToDamage(m_player, enemy, out damageRoll) )
                {
                    float damageApplied = damageRoll * 0.25f;

                    // equipment handling
                    // TODO shield check
                    var hitEquipment = enemy.m_equipment.GetRandomHittableEquipment(0.1f, 0.5f, 0.3f, 0.1f);
                    var playerWeapon = m_player.m_equipment.m_handPrimary;//.m_strength

                    float equipmentModifier = Mathf.Min(2f, 1f + (playerWeapon.m_strength - hitEquipment.m_strength) * 0.125f); // 1 +- diff in 0.125 incs max x2

                    damageApplied *= equipmentModifier;

                    // apply strike charges (1 + diff in 0.25 incs) max x5
                    damageApplied *= Mathf.Min(5, 1f + m_chargesPending * 0.25f);

                    damageApplied = Mathf.Min(damageApplied, enemy.m_health);

                    enemy.m_health -= damageApplied;

                    if(enemy.m_health <= 0)
                    {
                        if (m_chargesPending > 0)
                            m_log(string.Format("Quick strike kills {0} with {1} on {2} for <color=red>{3}</color>!", enemy.m_name, playerWeapon.m_name, hitEquipment.m_name, damageApplied.ToString("n2")));
                        else
                            m_log(string.Format("Fatal hit {0} with {1} on {2} for <color=orange>{3}</color>!", enemy.m_name, playerWeapon.m_name, hitEquipment.m_name, damageApplied.ToString("n2")));

                        m_enemies.Remove(enemy);

                        m_log(string.Format("{0} enemies remaining!", m_enemies.Count));
                    }
                    else
                    {
                        if(m_chargesPending > 0)
                            m_log(string.Format("Quick strike {0} with {1} on {2} for <color=red>{3}</color> health! [<color=blue>{4}</color>hp]", enemy.m_name, playerWeapon.m_name, hitEquipment.m_name, damageApplied.ToString("n2"), enemy.m_health.ToString("n2")));
                        else
                            m_log(string.Format("Hit {0} with {1} on {2} for <color=orange>{3}</color> health! [<color=blue>{4}</color>hp]", enemy.m_name, playerWeapon.m_name, hitEquipment.m_name, damageApplied.ToString("n2"), enemy.m_health.ToString("n2")));
                    }
                }
            }
        }

        // end fight check
        if(m_enemies.Count == 0)
        {
            EndCombat(true);
        }


        if (!m_playerTurn)
        {
            // handle dots.
            // calc damage with offense, player defense, and any status effects
            // apply player damage
        }


        // end fight check

        m_chargesPending = 0;
    }

    private void EndCombat(bool in_victorious)
    {
        m_onExit(in_victorious);
    }
}
