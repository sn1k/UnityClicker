﻿

public class DataPlayerStats
{
    internal int m_strength;
    internal int m_perception;
    internal int m_endurance;
    internal int m_agility;
    internal int m_luck;

    public DataPlayerStats(int in_strength, int in_perception, int in_endurance, int in_agility, int in_luck)
    {
        m_strength = in_strength;
        m_perception = in_perception;
        m_endurance = in_endurance;
        m_agility = in_agility;
        m_luck = in_luck;
    }

}
