﻿

internal class DataStatusEffect
{
    internal enum StatusEffectType
    {
        DamageOverTime,
        HealOverTime,
        BrokenArm,
        BrokenLeg,
        Blinded,
        ModifyStrength,
        ModifyPerception,
        ModifyEndurance,
        ModifyAgility,
        ModifyLuck,
        NoEffect
    };


    internal int m_durationTicks = 0;
    internal int m_strength = 1;

    internal StatusEffectType m_type;


    internal DataStatusEffect()
    {
        m_type = StatusEffectType.NoEffect;
        m_strength = 0;
        m_durationTicks = 0;
    }

    internal DataStatusEffect(StatusEffectType in_type, int in_strength, int in_durationTicks)
    {
        m_type = in_type;
        m_strength = in_strength;
        m_durationTicks = in_durationTicks;
    }

}
