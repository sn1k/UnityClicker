﻿
using System.Collections.Generic;
using UnityEngine;

public class DataActiveStatusEffects
{
    List<DataStatusEffect> m_effects = new List<DataStatusEffect>();

    internal void TickLifetimes()
    {
        foreach(var effect in m_effects)
        {
            if (--effect.m_durationTicks <= 0)
            {
                m_effects.Remove(effect);
            }
        }
    }

    internal void AttemptToApplyEffect(DataStatusEffect in_effect)
    {
        // up blind strength to 2, disregard additional
        // up broken limbs to 2 disregard additional
        // check for overriding dots, hots
    }

}
