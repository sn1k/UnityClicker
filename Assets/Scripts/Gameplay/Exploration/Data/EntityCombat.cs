﻿using System.Collections;
using System.Collections.Generic;

public class EntityCombat
{
    internal DataPlayerStats m_stats;
    internal DataActiveStatusEffects m_combatStatusEffects = new DataActiveStatusEffects();
    internal DataEquipment m_equipment;

    internal float m_health;
    internal float m_healthMax;
    internal string m_name;

    // loot stuff


    internal EntityCombat(DataPlayerStats in_stats, DataEquipment in_equipment, string in_name, float in_health)
    {
        m_stats = in_stats;
        m_equipment = in_equipment;
        m_health = in_health;
        m_healthMax = in_health;
        m_name = in_name;
    }

}
