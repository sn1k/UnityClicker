﻿
public class ItemEquipable : ItemLootable
{
    internal enum Slot {Head, Arms, Chest, Legs, Boots, HandPrimary, HandSecondary, NotEquipable };

    internal Slot m_equipableSlot = Slot.NotEquipable;

    internal float m_strength = 0f;

    internal ItemEquipable()
    {
        m_name = "unequipped";
        m_equipableSlot = Slot.NotEquipable;
        m_effect = new DataStatusEffect();
        m_strength = 0f;
        m_durability = 0f;
    }

    internal ItemEquipable(string in_name, Slot in_slot, float in_strength, DataStatusEffect in_statusEffect)
    {
        m_name = in_name;
        m_strength = in_strength;
        m_equipableSlot = in_slot;
        m_effect = in_statusEffect;

        m_durability = 1f;
    }
}
