﻿
using UnityEngine;

public class DataEquipment
{
    internal ItemEquipable m_head = new ItemEquipable();
    internal ItemEquipable m_chest = new ItemEquipable();
    internal ItemEquipable m_legs = new ItemEquipable();
    internal ItemEquipable m_boots = new ItemEquipable();
    internal ItemEquipable m_handPrimary = new ItemEquipable();
    internal ItemEquipable m_handSecondary = new ItemEquipable();

    internal ItemEquipable GetRandomHittableEquipment(float in_chanceHead, float in_chanceChest, float in_chanceLegs, float in_chanceFeet)
    {
        float sum = in_chanceChest + in_chanceFeet + in_chanceHead + in_chanceLegs;

        float randRoll = Random.Range(0, sum);

        if (randRoll < in_chanceFeet)
            return m_boots;

        randRoll -= in_chanceFeet; // drop roll by roulette segment so we don't have to test against cumulative

        if (randRoll < in_chanceHead)
            return m_head;

        randRoll -= in_chanceHead;

        if (randRoll < in_chanceLegs)
            return m_legs;
                
        return m_chest;
    }
}
