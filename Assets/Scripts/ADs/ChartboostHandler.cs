﻿
using ChartboostSDK;
using UnityEngine;


public class ChartboostHandler : MonoBehaviour
{

    private void Start()
    {
#if UNITY_IPHONE
        Chartboost.CreateWithAppId("593dc593f6cd45299f49401e", "dcdbed55beb1c85767a62b63bdf0b9c826ca943a");
#elif UNITY_ANDROID
        Chartboost.CreateWithAppId("593dc593f6cd45299f494020", "ec33a50a5889e3ed5ea0032f4a7dddfaa868743a");
#else
        Chartboost.Create();
#endif
        Chartboost.setAutoCacheAds(true);
        Chartboost.setMediation(CBMediation.AdMob, "1.0");

        Chartboost.didCompleteRewardedVideo += didCompleteRewardedVideo;

        Debug.Log("Chartboost: Creating with app id");


        Chartboost.cacheRewardedVideo(CBLocation.Default);
    }
    
    public void RewardVideoShow()
    {
        Debug.Log("Chartboost: Showing Video");
        Chartboost.showRewardedVideo(CBLocation.Default);
	}
    
    private void didCompleteRewardedVideo(CBLocation location, int reward)
    {
        Debug.Log(string.Format("didCompleteRewardedVideo: reward {0} at location {1}", reward, location));
    }
    
}
