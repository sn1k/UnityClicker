﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class UIInventoryDragHandler : MonoBehaviour, IBeginDragHandler, IDragHandler, IEndDragHandler
{
    internal Image m_image;
    private Canvas m_canvas;
    private CanvasGroup m_canvasGroup;
    private Vector2 m_selectionOffset;

    private Vector2 m_initialWorldPosition;

    private Transform m_initialParent;

    // Use this for initialization
    private void Start ()
    {
        m_canvas = (Canvas)gameObject.GetComponent(typeof(Canvas));
        m_canvasGroup = (CanvasGroup)gameObject.GetComponent(typeof(CanvasGroup));
        //todo assert
    }
	
	// Update is called once per frame
	private void Update ()
    {
		
	}

    public void OnBeginDrag(PointerEventData eventData)
    {
        m_initialParent = transform.parent;

        m_initialWorldPosition = gameObject.transform.position;
        m_selectionOffset = Input.mousePosition - gameObject.transform.position;

        m_canvasGroup.blocksRaycasts = false;

        m_canvas.sortingOrder = 3;
    }

    public void OnDrag(PointerEventData eventData)
    {
        Vector3 mouse = Input.mousePosition;
        
        gameObject.transform.position = new Vector2(mouse.x, mouse.y) - m_selectionOffset;
        
    }

    public void OnEndDrag(PointerEventData eventData)
    {
        m_canvasGroup.blocksRaycasts = true;

        if(m_initialParent  == transform.parent)
        {
            gameObject.transform.position = m_initialWorldPosition;
        }

        m_canvas.sortingOrder = 2;
    }
}
