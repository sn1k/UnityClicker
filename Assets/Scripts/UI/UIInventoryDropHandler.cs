﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class UIInventoryDropHandler : MonoBehaviour, IDropHandler
{
    // Use this for initialization
    private void Start()
    {
		
	}

    // Update is called once per frame
    private void Update()
    {
		
	}

    public void OnDrop(PointerEventData eventData)
    {
        UIInventoryDragHandler draggedObject = (UIInventoryDragHandler)eventData.pointerDrag.GetComponent(typeof(UIInventoryDragHandler));

        if(draggedObject == null)
        {
            Debug.Log("draggedObject was null " + gameObject.name);
            return;
        }

        draggedObject.transform.parent = gameObject.transform;
        //draggedObject.m_image.transform.localPosition = Vector3.zero;
        draggedObject.transform.position = gameObject.transform.position;

        Debug.Log("drop " + gameObject.name);

        // todo handle handoff from dragged to drop slot and gameplay changes
    }
}
