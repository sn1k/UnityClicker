﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ButtonHandlerGameplay : MonoBehaviour
{
    public void OnPressBuildProduct0()
    {
        Debug.Log("You have clicked the button!");

        ManagerGenerators.GetInstance().CreateGeneratorCurrency(Logistics.GeneratorTypes.Type.GOLD_MINOR);
    }

    public void OnPressBuildProduct1()
    {
        Debug.Log("You have clicked the button!");
        ManagerGenerators.GetInstance().CreateGeneratorCurrency(Logistics.GeneratorTypes.Type.GOLD_MAJOR);
    }

    public void OnPressBuildProduct2()
    {
        Debug.Log("You have clicked the button!");
        ManagerGenerators.GetInstance().CreateGeneratorCurrency(Logistics.GeneratorTypes.Type.GOLD_XXL);
    }

    public void OnPressBuildProduct3()
    {
        Debug.Log("You have clicked the button!");
        ManagerGenerators.GetInstance().CreateGeneratorCurrency(Logistics.GeneratorTypes.Type.PREMIUM_MINOR);
    }
}
