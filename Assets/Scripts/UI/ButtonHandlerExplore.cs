﻿using UnityEngine;

public class ButtonHandlerExplore : MonoBehaviour
{

    private ManagerGameplay m_manager;

    internal void RegisterManagerGameplay(ManagerGameplay in_gameplayManager)
    {
        m_manager = in_gameplayManager;
    }

    public void OnPressCharge()
    {
        m_manager.Charge();
    }

    public void OnPressUse()
    {
        m_manager.Strike();
    }
    
    public void OnPressPause()
    {
        m_manager.Pause();
    }

    public void OnPressShop()
    {
    }
    
    public void OnPressAds()
    {
        m_manager.ShowAds();
    }
}
