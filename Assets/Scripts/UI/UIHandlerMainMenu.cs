﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIHandlerMainMenu : MonoBehaviour
{
    private static UIHandlerMainMenu s_instance = null;

    private Text m_labelCurrency0;
    private Text m_labelCurrency1;

    private Text m_labelNumPurchasedProduct0;
    private Text m_labelNumPurchasedProduct1;
    private Text m_labelNumPurchasedProduct2;
    private Text m_labelNumPurchasedProduct3;

    private Dictionary<Logistics.GeneratableItemTypes.Type, Text> m_currencyTextMap = new Dictionary<Logistics.GeneratableItemTypes.Type, Text>();
    private Dictionary<Logistics.GeneratorTypes.Type, Text> m_generatorTextMap = new Dictionary<Logistics.GeneratorTypes.Type, Text>();


    private void Start()
    {
        if(s_instance != null)
        {
            Debug.LogError("UIHandlerMainMenu singleton instance already assigned, got 2 instances of UIHandlerMainMenu?");
            return;
        }

        s_instance = this;

        //grab currency labels
        m_labelNumPurchasedProduct0 = GetTextLabelForObjectName("TextNumPurchasedProduct0");
        m_labelNumPurchasedProduct1 = GetTextLabelForObjectName("TextNumPurchasedProduct1");
        m_labelNumPurchasedProduct2 = GetTextLabelForObjectName("TextNumPurchasedProduct2");
        m_labelNumPurchasedProduct3 = GetTextLabelForObjectName("TextNumPurchasedProduct3");
        m_labelCurrency0 = GetTextLabelForObjectName("TextCurrency0");
        m_labelCurrency1 = GetTextLabelForObjectName("TextCurrency1");

        AssignTextMappings();

        // subscribe for currency changed events
        ManagerGenerators.GetInstance().SubscribeCurrencyChangedEvent(UpdateCurrencyLabel);

        ManagerGenerators.GetInstance().SubscribeGeneratorsChangedEvent(UpdateGeneratorLabels);
    }

    private void AssignTextMappings()
    {
        m_currencyTextMap[Logistics.GeneratableItemTypes.Type.GOLD] = m_labelCurrency0;
        m_currencyTextMap[Logistics.GeneratableItemTypes.Type.PREMIUM] = m_labelCurrency1;

        m_generatorTextMap[Logistics.GeneratorTypes.Type.GOLD_MINOR] = m_labelNumPurchasedProduct0;
        m_generatorTextMap[Logistics.GeneratorTypes.Type.GOLD_MAJOR] = m_labelNumPurchasedProduct1;
        m_generatorTextMap[Logistics.GeneratorTypes.Type.GOLD_XXL] = m_labelNumPurchasedProduct2;
        m_generatorTextMap[Logistics.GeneratorTypes.Type.PREMIUM_MINOR] = m_labelNumPurchasedProduct3;
    }

    internal void UpdateCurrencyLabel(Logistics.GeneratableItemTypes.Type in_type, double in_amount)
    {
        m_currencyTextMap[in_type].text = in_amount.ToString("F3");
    }

    internal void UpdateGeneratorLabels(Logistics.GeneratorTypes.Type in_type, int in_amount)
    {
        m_generatorTextMap[in_type].text = in_amount.ToString();
    }
    

    internal static UIHandlerMainMenu GetInstance()
    {
        if (s_instance == null)
            Debug.LogError("UIHandlerMainMenu singleton instance not yet assigned!");

        return s_instance;
    }

    private static Text GetTextLabelForObjectName(string in_name)
    {
        GameObject ob = GameObject.Find(in_name);

        if (ob == null)
        {
            Debug.LogError("GetTextLabelForObjectName couldn't find game object named " + in_name);
            return null;
        }

        Text label = (Text)ob.GetComponent(typeof(Text));

        if(label == null)
            Debug.LogError("GetTextLabelForObjectName couldn't find Text component on object named " + in_name);
        
        return label;
    }


    
}

